import random
from math import sin, cos, acos, sqrt
import os

import numpy as np
import pandas as pd

from poi import POI


def dfs(p, points, visits, i, d, t_max, end):
    visits[p.pk] = True

    if i == d:
        if get_eucl_dist(p, end) <= t_max[i]:
            return True, [p, end]
        else:
            return False, []

    for candidate in random.sample(points.tolist(), len(points)):
        if visits[candidate.pk] or get_eucl_dist(p, candidate) > t_max[i]:
            continue
        result, data = dfs(candidate, points, list(visits), i + 1, d, t_max, end)

        if result:
            return result, [p] + data

    return False, []


def validate_route(route, t_max, d):
    actual_distance = 0
    total_chargers = 1
    unique_pois = []

    distances = []

    for i in range(1, len(route)):
        x = route[i - 1]
        y = route[i]
        actual_distance += sqrt((x[0] - y[0]) ** 2 + (x[1] - y[1]) ** 2)

        if route[i][2] == 0.0:
            total_chargers += 1
            distances.append(actual_distance)
            actual_distance = 0
        else:
            unique_pois.append(str(route[i]))

    for a, b in zip(distances, t_max):
        if a > b:
            print('DIST EXCEEDED')
            print('distances', distances)
            print('t_max', t_max)
            return False

    if not len(set(unique_pois)) == len(unique_pois):
        print('not unique points')
        return False

    if total_chargers != d + 2:
        print('invalid chargers count')
        return False

    return True


def execute_benchmark(algorithm, data, index=None):
    basedir = 'C:\\Users\\Karnemelk\\Desktop\OPHS instances_February 2013'

    results = []

    files = get_files(basedir, index=index)

    for i, file in enumerate(files, 1):
        start = 0
        end = 1
        t_max, d, df, total = read_single_benchmark(file)

        data.update({
            'start': start,
            'end': end,
            'd': d - 1,
            't_max': t_max,
            'df': df
        })

        result = algorithm(**data)

        # print(f'Validation result: {validate_route(result["tour"], t_max, d - 1)}')

        results.append(result['data'])

        # ratio = result["data"]["value"] / total * 100
        # print(f'{result["data"]["value"]}/{total} ({ratio:.2f}%)')

        # print(f'{i} / {len(files)} processed.')
    print(f'{len(files)} / {len(files)} processed.')

    return results


def get_files(basedir, index=None):
    files = []
    dirs = os.listdir(basedir)

    if index is not None:
        print(dirs[index])
        dirs = [dirs[index]]

    for dir in dirs:
        dir = os.path.join(basedir, dir)
        if not os.path.isdir(dir):
            continue
        for file in os.listdir(dir):
            file = os.path.join(dir, file)
            if os.path.isfile(file) and file.split('.')[-1] == 'ophs':
                files.append(file)
    return files


def read_single_benchmark(path):
    with open(path) as f:
        lines = [line.strip('\t').split('\t') for line in f.read().splitlines()]

    n = int(lines[0][0]) + 2
    d = int(lines[0][2])

    t_max = [float(value) for value in lines[2]]

    data = []
    keys = ['lat', 'lon', 'value']

    for i in range(4, n + 3):
        line = [float(value) for value in lines[i]]
        data.append(dict(zip(keys, line)))

    return t_max, d, pd.DataFrame(data), pd.DataFrame(data)['value'].sum()


def make_pois_from_df(df):
    chargin_stations = []
    pois = []

    chargers_index = 0
    pois_index = 0

    for _, item in df.iterrows():
        item = POI(
            pk=chargers_index if item.value == 0.0 else pois_index,
            lat=item.lat,
            lon=item.lon,
            value=item.value
        )

        if item.value == 0.0:
            chargin_stations.append(item)
            chargers_index += 1
        else:
            pois.append(item)
            pois_index += 1

    return np.array(pois), np.array(chargin_stations)


def get_distance(x, y):
    AP = 90 - x.lat
    BP = 90 - y.lat
    P = y.lon - x.lon

    cd = (acos(cos(AP) * cos(BP) + sin(AP) * sin(BP) * cos(P))) * 111.1
    return cd


def get_eucl_dist(x, y):
    return sqrt((x.lat - y.lat) ** 2 + (x.lon - y.lon) ** 2)


# def get_distance(point_a, point_b):
#     lat1 = point_a.lat * pi / 180
#     lon1 = point_a.lon * pi / 180
#     lat2 = point_b.lat * pi / 180
#     lon2 = point_b.lon * pi / 180
#
#     dlat = lat2 - lat1
#     dlon = lon2 - lon1
#
#     a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * (sin(dlon / 2) ** 2)
#     c = 2 * atan2(sqrt(a), sqrt(1 - a))
#     distance = 6371 * c
#
#     return distance


def if_points_equal(a, b):
    return a.value == b.value and a.lat == b.lat and a.lon == b.lon and a.pk == b.pk
