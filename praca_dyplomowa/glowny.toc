\babel@toc {polish}{}
\babel@toc {polish}{}
\contentsline {chapter}{Streszczenie}{5}{chapter*.2}
\contentsline {chapter}{Wst\k ep}{13}{chapter*.4}
\contentsline {chapter}{\numberline {1}Analiza i opis problemu}{15}{chapter.1}
\contentsline {chapter}{\numberline {2}Algorytm zach\IeC {\l }anny}{19}{chapter.2}
\contentsline {section}{\numberline {2.1}Opis algorytmu}{20}{section.2.1}
\contentsline {section}{\numberline {2.2}Operatory optymalizuj\k ace}{23}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Operator 2-opt}{23}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Operator zamiany}{25}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Operator wstawiania}{27}{subsection.2.2.3}
\contentsline {chapter}{\numberline {3}Algorytm genetyczny}{29}{chapter.3}
\contentsline {section}{\numberline {3.1}Opis algorytmu}{29}{section.3.1}
\contentsline {section}{\numberline {3.2}Populacja pocz\k atkowa}{31}{section.3.2}
\contentsline {section}{\numberline {3.3}Operatory}{34}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Krzy\.zowanie}{34}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Mutacja}{36}{subsection.3.3.2}
\contentsline {subsubsection}{Mutacja wstawiaj\k aca}{36}{lstnumber.8}
\contentsline {subsubsection}{Mutacja usuwaj\k aca}{38}{lstnumber.29}
\contentsline {subsection}{\numberline {3.3.3}Selekcja}{39}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Warunek stopu}{40}{subsection.3.3.4}
\contentsline {chapter}{\numberline {4}Algorytm hybrydowy}{41}{chapter.4}
\contentsline {section}{\numberline {4.1}Opis algorytmu}{41}{section.4.1}
\contentsline {section}{\numberline {4.2}Operator 2-opt}{43}{section.4.2}
\contentsline {chapter}{\numberline {5}Dane i eksperymenty}{45}{chapter.5}
\contentsline {section}{\numberline {5.1}Dane rzeczywiste}{45}{section.5.1}
\contentsline {section}{\numberline {5.2}Algorytm zach\IeC {\l }anny}{46}{section.5.2}
\contentsline {section}{\numberline {5.3}Algorytm genetyczny}{48}{section.5.3}
\contentsline {section}{\numberline {5.4}Algorytm hybrydowy}{48}{section.5.4}
\contentsline {section}{\numberline {5.5}Kalibracja parametr\'ow}{49}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Parametry algorytmu genetycznego i hybrydowego}{49}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Warunek stopu}{50}{subsection.5.5.2}
\contentsline {section}{\numberline {5.6}Wnioski z uzyskanych wynik\'ow}{51}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}Por\'ownanie}{51}{subsection.5.6.1}
\contentsline {subsection}{\numberline {5.6.2}Statystyki}{52}{subsection.5.6.2}
\contentsline {section}{\numberline {5.7}Testowanie poprawno\'sci}{54}{section.5.7}
\contentsline {section}{\numberline {5.8}Przyk\IeC {\l }adowe \'scie\.zki}{54}{section.5.8}
\contentsline {chapter}{\numberline {6}Eksperymenty na benchmarkach}{63}{chapter.6}
\contentsline {section}{\numberline {6.1}Dane}{63}{section.6.1}
\contentsline {section}{\numberline {6.2}Generowanie trasy pocz\k atkowej}{63}{section.6.2}
\contentsline {section}{\numberline {6.3}Algorytm zach\IeC {\l }anny}{65}{section.6.3}
\contentsline {section}{\numberline {6.4}Algorytm genetyczny}{65}{section.6.4}
\contentsline {section}{\numberline {6.5}Algorytm hybrydowy}{66}{section.6.5}
\contentsline {section}{\numberline {6.6}Kalibracja parametr\'ow}{66}{section.6.6}
\contentsline {section}{\numberline {6.7}Wyniki}{67}{section.6.7}
\contentsline {subsection}{\numberline {6.7.1}Por\'ownanie profitu algorytm\'ow}{69}{subsection.6.7.1}
\contentsline {subsection}{\numberline {6.7.2}Por\'ownanie czasu wykonania algorytm\'ow}{71}{subsection.6.7.2}
\contentsline {chapter}{\numberline {7}Z\IeC {\l }o\.zono\'s\'c algorytm\'ow}{73}{chapter.7}
\contentsline {chapter}{\numberline {8}Zastosowania}{75}{chapter.8}
\contentsline {chapter}{Podsumowanie}{77}{chapter*.48}
\contentsline {chapter}{Bibliografia}{80}{chapter*.49}
\contentsline {chapter}{Spis tabel}{81}{chapter*.50}
\contentsline {chapter}{Spis rysunk\'ow}{86}{chapter*.51}
\contentsline {chapter}{Spis listing\'ow}{87}{chapter*.52}
\contentsline {chapter}{\numberline {A}Dodatek - profit na benchmarkach}{89}{appendix.A}
\contentsline {chapter}{\numberline {B}Dodatek - czas wykonania na benchmarkach}{95}{appendix.B}
