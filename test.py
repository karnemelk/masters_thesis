import random
from math import sqrt, sin, cos, atan2, pi, acos

import numpy as np

from greedy.algorithm import run_algorithm as greedy
from genetic.algorithm import run_algorithm as genetic
from hybrid.algorithm import run_algorithm as hybrid


def greedy_distance(x, y):
    AP = 90 - x[0]
    BP = 90 - y[0]
    P = y[1] - x[1]

    cd = (acos(cos(AP) * cos(BP) + sin(AP) * sin(BP) * cos(P))) * 111.1
    return cd


def get_distance(point_a, point_b):
    lat1 = point_a[0] * pi / 180
    lon1 = point_a[1] * pi / 180
    lat2 = point_b[0] * pi / 180
    lon2 = point_b[1] * pi / 180

    dlat = lat2 - lat1
    dlon = lon2 - lon1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * (sin(dlon / 2) ** 2)
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = 6371 * c

    return distance


def validate_route(route, t_max, d, is_greedy):
    actual_distance = 0
    total_chargers = 1
    unique_pois = []

    for i in range(1, len(route)):
        if total_chargers >= d + 2:
            return False

        if is_greedy:
            actual_distance += greedy_distance(route[i - 1], route[i])
        else:
            actual_distance += get_distance(route[i - 1], route[i])

        if actual_distance > t_max[total_chargers - 1]:
            return False

        if route[i][2] == 0.0:
            total_chargers += 1
            actual_distance = 0
        else:
            unique_pois.append(str(route[i]))

    if not len(set(unique_pois)) == len(unique_pois):
        return False

    if total_chargers != d + 2:
        return False

    return True


def test_random_algorithm(algorithm, is_greedy=False):
    m_list = list(range(100, 300))
    d_list = list(range(3))
    p_k = 0.4
    p_m = 0.4

    passed = 0
    failed = 0

    for test in range(100):
        d = random.choice(d_list)
        m = [random.choice(m_list)] * (d + 1)
        if is_greedy:
            input = {'start': 0, 'end': 2, 'd': d, 'm': m, 'modifier': 1.2, 'path': 'data.csv'}
        else:
            input = {'m': m, 'd': d, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': 'data.csv'}

        data = algorithm(**input)

        result = validate_route(data["tour"], m, d, is_greedy)

        if result:
            passed += 1
        else:
            failed += 1

        print(f'Test {test}: {"[ passed ]" if result else "[ failed ]"}')

    return passed, failed


if __name__ == '__main__':
    print('\nPerform tests for greedy algorithm:\n')
    greedy_passed, greedy_failed = test_random_algorithm(greedy, is_greedy=True)
    print('\nPerform tests for genetic algorithm:\n')
    genetic_passed, genetic_failed = test_random_algorithm(genetic)
    print('\nPerform tests for hybrid algorithm:\n')
    hybrid_passed, hybrid_failed = test_random_algorithm(hybrid)

    print('\nGreedy tests summary')
    print(f'passed: [{greedy_passed}/{100}]')
    print(f'failed: [{greedy_failed}/{100}]')

    print('\nGenetic tests summary')
    print(f'passed: [{genetic_passed}/{100}]')
    print(f'failed: [{genetic_failed}/{100}]')

    print('\nHybrid tests summary')
    print(f'passed: [{hybrid_passed}/{100}]')
    print(f'failed: [{hybrid_failed}/{100}]')
