class POI:
    def __init__(self, pk, lat, lon, value):
        self.pk = pk
        self.lat = lat
        self.lon = lon
        self.value = value

    def __str__(self):
        # poi_type = 'charger' if self.value == 0.0 else 'poi'
        # return '{} #{} = {}'.format(poi_type, self.pk, self.value)
        return '#{}. at ({}, {}) = {}'.format(self.pk, self.lat, self.lon, self.value)

    def __repr__(self):
        return '#{}. at ({}, {}) = {}'.format(self.pk, self.lat, self.lon, self.value)
