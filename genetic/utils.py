import numpy as np

from greedy.algorithm import get_distances
from global_utils import get_eucl_dist as get_distance, dfs
from greedy.utils import perform_for_section


def get_initial_greedy(start, end, d, t_max, pois, chargers, modifier, with_2opt=False):
    attractions_to_attractions = get_distances(pois, pois, modifier)
    chargers_to_attractions = get_distances(chargers, pois, modifier)

    tour = []

    is_poi_visited = [False] * len(pois)
    is_charger_visited = [False] * len(chargers)

    first_charger = chargers[start]
    last_charger = chargers[end]

    chargers = np.delete(chargers, max(start, end))
    chargers = np.delete(chargers, min(start, end))

    result, ready_chargers = dfs(first_charger, chargers, is_charger_visited, 0, d, t_max, last_charger)

    if not result:
        return []

    # [ MAIN ALGORITHM ]
    for i in range(d + 1):
        trip = [ready_chargers[i], ]
        last_charger = ready_chargers[i + 1]
        recently_charged = True
        actual = trip[0]
        part = 0

        while True:
            # [ GET CANDIDATE POINT ]
            j = 0

            try:
                if recently_charged:
                    while is_poi_visited[chargers_to_attractions[actual.pk][j]['point'].pk]:
                        j += 1
                    candidate = chargers_to_attractions[actual.pk][j]['point']
                    recently_charged = False
                else:
                    while is_poi_visited[attractions_to_attractions[actual.pk][j]['point'].pk]:
                        j += 1
                    candidate = attractions_to_attractions[actual.pk][j]['point']
            except IndexError:
                break

            dist_left = get_distance(actual, candidate)
            dist_right = get_distance(candidate, last_charger)

            # [ CHECK VALIDATION ]
            if part + dist_left + dist_right <= t_max[i]:
                part += dist_left

                is_poi_visited[candidate.pk] = True
                trip.append(candidate)

                actual = candidate

                # [ 2-OPT OPERATOR EXECUTION ]
                if with_2opt:
                    trip, part = perform_for_section(trip, part)
            else:
                break

        part += get_distance(trip[-1], last_charger)
        trip.append(last_charger)

        # [ 2-OPT OPERATOR EXECUTION ]
        if with_2opt:
            trip, part = perform_for_section(trip, part)

        tour.append(trip)

    return tour
