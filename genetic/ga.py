import random
from copy import deepcopy

import numpy as np

from math import sin, cos, acos, sqrt

from genetic.utils import get_initial_greedy


class GA:
    def __init__(self, start, end, t_max, d, pois, chargers, modifier, ready_tour=None, raw=None, with_2opt=False, threshold=0.9):
        self.t_max = t_max
        self.d = d
        self.modifier = modifier
        self.threshold = threshold

        self.pois, self.chargers = deepcopy(pois), deepcopy(chargers)
        self.pois_onehot = np.zeros(len(pois), dtype=bool)

        if raw:
            self.route = raw
            self.tours_distances = [self.get_tour_distance(tour) for tour in self.route]
            self.distance = sum(self.tours_distances)
            self.value = 0
            for tour in self.route:
                for poi in tour:
                    self.value += poi.value
                    self.pois_onehot[poi.pk] = True
        else:
            self.route = []
            self.tours_distances = []
            self.value = 0

            if ready_tour:
                # tour = get_initial_greedy(start, end, d, t_max, pois, chargers, modifier, with_2opt=with_2opt)

                # if tour:
                for i in range(d + 1):
                    for point in ready_tour[i]:
                        if point.value != 0:
                            self.pois_onehot[point.pk] = True
                            self.value += point.value
                    self.route.append(list(ready_tour[i]))
                    self.tours_distances.append(self.get_tour_distance(ready_tour[i]))

    def check(self):
        unique_pois = []
        for tour in self.route:
            for i in range(1, len(tour) - 1):
                poi = tour[i]
                unique_pois.append(poi.pk)

        if not len(set(unique_pois)) == len(unique_pois):
            print('\nCHECK:')

            print('unique pois:', len(set(unique_pois)) == len(unique_pois))
            print(sorted(unique_pois))

            unique, counts = np.unique(self.pois_onehot, return_counts=True)
            print(dict(zip(unique, counts)))
            print('pois in route:', sum([len(tour) - 2 for tour in self.route]))
            print('pois unused:', len(self.pois))

    # def create_single_tour(self, i):
    #     is_last = i == self.d
    #
    #     tour = np.array([self.first_charger, ])
    #
    #     total = 0
    #     selected_charger = None
    #
    #     while True:
    #         actual_pois = self.pois[~self.pois_onehot]
    #
    #         index_attr = random.randint(0, len(actual_pois) - 1)
    #         attraction = actual_pois[index_attr]
    #
    #         if is_last:
    #             index_charge = None
    #             charger = self.last_charger
    #         else:
    #             index_charge = self.get_nearest_charger(attraction, self.chargers, index=True)
    #             charger = self.chargers[index_charge]
    #
    #         distance_left = self.get_distance(tour[-1], attraction)
    #         distance_right = self.get_distance(attraction, charger)
    #
    #         if total + distance_left + distance_right < self.t_max[i]:
    #             total += self.get_distance(tour[-1], attraction)
    #             selected_charger = charger
    #             tour = np.append(tour, attraction)
    #             self.value += attraction.value
    #
    #             self.pois_onehot[attraction.pk] = True
    #
    #             self.last_charger_index = index_charge
    #         else:
    #             if selected_charger is None:
    #                 if is_last:
    #                     selected_charger = self.last_charger
    #                 else:
    #                     index_charge = self.get_nearest_charger(attraction, self.chargers, index=True)
    #                     selected_charger = self.chargers[index_charge]
    #                     self.last_charger_index = index_charge
    #             break
    #
    #     tour = np.append(tour, selected_charger)
    #
    #     self.first_charger = selected_charger
    #
    #     if not is_last:
    #         self.chargers = np.delete(self.chargers, self.last_charger_index)
    #
    #     self.tours_distances.append(self.get_tour_distance(tour))
    #
    #     return tour

    # def get_nearest_charger(self, attraction, stations, index=False):
    #     distance = 1e9
    #     nearest = None
    #     idx = None
    #
    #     for i in range(len(stations)):
    #         station = stations[i]
    #         temp_dist = self.get_distance(attraction, station)
    #
    #         if temp_dist < distance:
    #             distance = temp_dist
    #             nearest = station
    #             idx = i
    #
    #     if index:
    #         return idx
    #
    #     return nearest

    def get_route_distance(self):
        return sum([self.get_tour_distance(tour) for tour in self.route])

    def get_tour_distance(self, tour):
        return sum([self.get_distance(tour[i - 1], tour[i]) for i in range(1, len(tour))])

    # def get_distance(self, x, y):
    #     ap = 90 - x.lat
    #     bp = 90 - y.lat
    #     p = y.lon - x.lon
    #
    #     cd = (acos(cos(ap) * cos(bp) + sin(ap) * sin(bp) * cos(p))) * 111.1
    #     return cd

    def get_distance(self, x, y):
        return sqrt((x.lat - y.lat) ** 2 + (x.lon - y.lon) ** 2)

    def fitness(self):
        total_length = sum(self.tours_distances)
        return self.value ** self.modifier / total_length

    def make_mutation(self, proba):
        if proba < self.threshold:
            self.insert_mutation()
        else:
            self.remove_mutation()

    def insert_mutation(self):
        actual_pois = self.pois[~self.pois_onehot]
        if actual_pois.shape[0] == 0:
            return

        attraction_index = random.randint(0, len(actual_pois) - 1)
        candidate = actual_pois[attraction_index]

        best_tour_index = None
        best_position_index = None
        best_candidate_fitness = 0

        for i in range(len(self.route)):
            tour = self.route[i]
            distance = self.tours_distances[i]

            for j in range(1, len(tour)):
                distance_given = self.get_distance(tour[j - 1], candidate) + self.get_distance(candidate, tour[j])
                distance_removed = self.get_distance(tour[j - 1], tour[j])

                if distance - distance_removed + distance_given <= self.t_max[i]:
                    try:
                        value = (candidate.value ** self.modifier) / distance_given
                        if value > best_candidate_fitness:
                            best_candidate_fitness = value
                            best_tour_index = i
                            best_position_index = j
                    except ZeroDivisionError:
                        continue

        if best_position_index:
            self.pois_onehot[candidate.pk] = True
            self.value += candidate.value
            self.route[best_tour_index] = np.insert(self.route[best_tour_index], best_position_index, candidate)

            self.tours_distances[best_tour_index] = self.get_tour_distance(self.route[best_tour_index])

            return True
        return False

    def remove_mutation(self):
        lowest_tour_index = None
        lowest_position_index = None
        lowest_candidate_fitness = 1e9

        for i in range(len(self.route)):
            tour = self.route[i]

            for j in range(1, len(tour) - 1):
                distance_left = self.get_distance(tour[j - 1], tour[j])
                distance_right = self.get_distance(tour[j], tour[j + 1])
                value = tour[j].value
                dist = distance_left + distance_right or 1
                fitness = value ** self.modifier / dist

                if fitness < lowest_candidate_fitness:
                    lowest_tour_index = i
                    lowest_position_index = j
                    lowest_candidate_fitness = fitness

        if lowest_position_index:
            lowest_poi = self.route[lowest_tour_index][lowest_position_index]
            self.route[lowest_tour_index] = np.delete(self.route[lowest_tour_index], lowest_position_index, axis=0)

            self.value = self.value - lowest_poi.value
            self.pois_onehot[lowest_poi.pk] = False
            self.tours_distances[lowest_tour_index] = self.get_tour_distance(self.route[lowest_tour_index])

            return True
        return False
