import random
from math import sqrt, sin, cos, atan2, pi

import numpy as np

from genetic.ga import GA


def get_distance(x, y):
    return sqrt((x.lat - y.lat) ** 2 + (x.lon - y.lon) ** 2)


# def get_distance(point_a, point_b):
#     lat1 = point_a.lat * pi / 180
#     lon1 = point_a.lon * pi / 180
#     lat2 = point_b.lat * pi / 180
#     lon2 = point_b.lon * pi / 180
#
#     dlat = lat2 - lat1
#     dlon = lon2 - lon1
#
#     a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * (sin(dlon / 2) ** 2)
#     c = 2 * atan2(sqrt(a), sqrt(1 - a))
#     distance = 6371 * c
#
#     return distance


def get_parents_to_crossover(population, p_k):
    id_list = list(range(len(population)))
    total_max = int(len(population) * p_k)
    if total_max % 2 != 0:
        total_max = max(0, total_max - 1)
    parents_to_crossing = []

    for i in range(int(total_max / 2)):
        idxA = random.randint(0, len(id_list) - 1)
        pA = population[id_list.pop(idxA)]
        idxB = random.randint(0, len(id_list) - 1)
        pB = population[id_list.pop(idxB)]

        parents_to_crossing.append((pA, pB, idxA, idxB))

    return parents_to_crossing


def make_crossover(population, p_a, p_b, idx_a, idx_b, t_max, d, all_pois, all_chargers, modifier):
    if p_a.value < p_b.value:
        lowest_index = idx_a
        lowest_value = p_a.value
    else:
        lowest_index = idx_b
        lowest_value = p_b.value

    new_pA, new_pB = get_random_crossover(p_a, p_b, t_max, d, all_pois, all_chargers, modifier)

    best_new = None

    if new_pA:
        best_new = new_pA

    if new_pB and best_new is None:
        best_new = new_pB
    elif new_pB and best_new and new_pB.value > best_new.value:
        best_new = new_pB

    if best_new and best_new.value > lowest_value:
        population[lowest_index] = best_new

    return population


def get_pk_for_place(item):
    return '{}_{}'.format('charger' if item.pk == 0.0 else 'poi', item.pk)


def get_random_crossover(p_a, p_b, t_max, d, all_pois, all_chargers, modifier):
    pA_route = [p_a.route[0][0]]
    pB_route = [p_b.route[0][0]]

    for tour in p_a.route:
        pA_route.extend(tour[1:])

    for tour in p_b.route:
        pB_route.extend(tour[1:])

    pk_list_A = [get_pk_for_place(item) for item in pA_route[1: -1]]
    pk_list_B = [get_pk_for_place(item) for item in pB_route[1: -1]]

    inter = list(set(pk_list_A).intersection(pk_list_B)) or None

    if not inter:
        return None, None

    random_poi = random.choice(inter)

    pA_index = pk_list_A.index(random_poi) + 1
    pB_index = pk_list_B.index(random_poi) + 1

    pA_left = pA_route[: pA_index]
    pA_right = pA_route[pA_index:]
    pB_left = pB_route[: pB_index]
    pB_right = pB_route[pB_index:]

    new_pA = pA_left + pB_right
    new_pB = pB_left + pA_right

    output = [None, None]

    if validate_new_route(new_pA, pA_route, pB_route, t_max, d):
        new_pA_route = []
        temp_tour = None

        for item in new_pA:
            if item.value == 0.0:
                if not temp_tour:
                    temp_tour = [item]
                else:
                    temp_tour.append(item)
                    new_pA_route.append(np.array(temp_tour))
                    temp_tour = [item]
            else:
                temp_tour.append(item)

        new_pA = GA(0, 0, t_max, d, all_pois, all_chargers, modifier, raw=new_pA_route)

        output[0] = new_pA
    if validate_new_route(new_pB, pA_route, pB_route, t_max, d):
        new_pB_route = []
        temp_tour = None

        for item in new_pB:
            if item.value == 0.0:
                if not temp_tour:
                    temp_tour = [item]
                else:
                    temp_tour.append(item)
                    new_pB_route.append(np.array(temp_tour))
                    temp_tour = [item]
            else:
                temp_tour.append(item)

        new_pB = GA(0, 0, t_max, d, all_pois, all_chargers, modifier, raw=new_pB_route)

        output[1] = new_pB

    return output


def validate_new_route(route, old_p_a, old_p_b, t_max, d):
    actual_distance = 0
    total_chargers = 1
    unique_pois = []

    for i in range(1, len(route)):
        if total_chargers >= d + 2:
            return False

        actual_distance += get_distance(route[i - 1], route[i])

        if actual_distance > t_max[total_chargers - 1]:
            return False

        if route[i].value == 0.0:
            total_chargers += 1
            actual_distance = 0
        else:
            unique_pois.append(route[i].pk)

    if not len(set(unique_pois)) == len(unique_pois):
        return False

    if total_chargers != d + 2:
        return False

    return if_routes_different(route, old_p_a) and if_routes_different(route, old_p_b)


def if_routes_different(new_route, old_route):
    if len(new_route) != len(old_route):
        return True
    else:
        for itemA, itemB in zip(new_route, old_route):
            if itemA.pk != itemB.pk:
                return True

    return False
