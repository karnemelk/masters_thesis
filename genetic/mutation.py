import random


def get_parents_to_mutation(population, p_m):
    id_list = list(range(len(population)))
    total_max = int(len(population) * p_m)
    parents_to_mutation = []

    for i in range(total_max):
        idx = random.randint(0, len(id_list) - 1)
        p = population[id_list.pop(idx)]

        parents_to_mutation.append(p)

    return parents_to_mutation
