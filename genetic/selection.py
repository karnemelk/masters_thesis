import random


def get_best_parent(parents):
    best_parent = None
    best_fitness = -1e9
    for parent in parents:
        fitness = parent.fitness()
        if fitness > best_fitness:
            best_fitness = fitness
            best_parent = parent

    return best_parent


def select_random_k(population, k):
    return [random.choice(population) for _ in range(k)]


def select_best_from_population(population, k):
    new_population = []
    for _ in range(len(population)):
        random_k = select_random_k(population, k)
        best = get_best_parent(random_k)

        new_population.append(best)
        # new_population.append(deepcopy(best))

    return new_population
