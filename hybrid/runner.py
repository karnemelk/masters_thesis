import pandas as pd

from hybrid.algorithm import run_algorithm


def run(single=True):
    p_m = 0.4
    p_k = 0.4

    input_data = [
        {'m': 120, 'd': 0, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': '../data.csv'},
        {'m': 150, 'd': 0, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': '../data.csv'},
        {'m': 170, 'd': 0, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': '../data.csv'},
        {'m': 210, 'd': 0, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': '../data.csv'},
        {'m': 420, 'd': 0, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': '../data.csv'},
        {'m': 120, 'd': 1, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': '../data.csv'},
        {'m': 150, 'd': 1, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': '../data.csv'},
        {'m': 170, 'd': 1, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': '../data.csv'},
        {'m': 210, 'd': 1, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': '../data.csv'},
        {'m': 420, 'd': 1, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': '../data.csv'}
    ]

    # keys = ['m', 'd', 'p_size', 't_size', 'n', 'iterations', 'fitness', 'distance', 'value', 'total_time']
    keys = ['m', 'd', 'iterations', 'fitness', 'distance', 'value', 'total_time']

    if single:
        m = 210  # max tour length
        d = 0  # loads amount
        p_size = 50  # population size (check 300)
        t_size = 5  # number of individuals
        n = 100  # GA generations
        path = '../data.csv'

        options = [run_algorithm(m, d, p_size, t_size, n, p_m, p_k, path) for _ in range(5)]
        best_val = 0
        result = None

        for item in options:
            if item['data']['value'] > best_val:
                best_val = item['data']['value']
                result = item

        df = pd.DataFrame([result['data']], columns=keys)
        print(df)
    else:
        results = []
        for input in input_data:
            options = [run_algorithm(**input) for _ in range(5)]
            best_val = 0
            result = None

            for item in options:
                if item['data']['value'] > best_val:
                    best_val = item['data']['value']
                    result = item

            print('\nbest value: {}\n'.format(result['data']['value']))

            results.append(result['data'])

        df = pd.DataFrame(results, columns=keys)
        # df.to_csv('results_hybrid.csv')
        print(df)

        print('-- processing complete, results saved to the file --')


if __name__ == '__main__':
    run(single=False)
