from genetic.ga import GA


class Hybrid(GA):
    def make_mutation(self, proba):
        result = self.insert_mutation() if proba < self.threshold else self.remove_mutation()
        if result:
            self.perform_2opt()

    def perform_2opt(self):
        for i in range(len(self.route)):
            tour = self.route[i]

            if len(tour) < 4:
                continue

            for j in range(3, len(tour)):
                tour = self.perform_for_section(tour, j)

            self.route[i] = tour

    def perform_for_section(self, tour, index):
        if len(tour) < 3:
            return tour

        last1 = index - 1
        last2 = index

        for i in range(index - 2):
            first1 = i
            first2 = i + 1

            dist_old_1 = self.get_distance(tour[first1], tour[first2])
            dist_old_2 = self.get_distance(tour[last1], tour[last2])

            dist_new_1 = self.get_distance(tour[first1], tour[last1])
            dist_new_2 = self.get_distance(tour[first2], tour[last2])

            if dist_old_1 + dist_old_2 > dist_new_1 + dist_new_2:
                i_left = int(first2)
                i_right = int(last1)
                while i_left < i_right:
                    temp = tour[i_left]
                    tour[i_left] = tour[i_right]
                    tour[i_right] = temp

                    i_left += 1
                    i_right -= 1

                break

        return tour
