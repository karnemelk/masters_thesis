import random
from datetime import datetime

import numpy as np
import pandas as pd

from genetic.crossover import get_parents_to_crossover, make_crossover
from genetic.mutation import get_parents_to_mutation
from genetic.selection import get_best_parent, select_best_from_population
from genetic.utils import get_initial_greedy
from global_utils import make_pois_from_df, dfs
from hybrid.ha import Hybrid


def run_algorithm(t_max, d, p_size, t_size, n, p_m, p_k, modifier, path, threshold, start=0, end=2, df=None):
    if df is not None:
        pois, chargers = make_pois_from_df(df)
    else:
        pois, chargers = make_pois_from_df(pd.read_csv(path))

    now = datetime.now()

    ready_tour = get_initial_greedy(start, end, d, t_max, pois, chargers, modifier, with_2opt=True)

    population = [Hybrid(start, end, t_max, d, pois, chargers, modifier, ready_tour=ready_tour, with_2opt=True, threshold=threshold) for _ in range(p_size)]

    if not population[0].route:
        print('Tour cannot be created!')
        return {'data': {'value': 0, 'time': datetime.now() - now}, 'tour': []}

    best_parent = get_best_parent(population)
    best_values = [
        (best_parent.value, best_parent.fitness(), best_parent.get_route_distance(), best_parent.route.copy())
    ]

    best_value = 0
    counter = 0

    for i in range(n):
        # [ SELECTION ]
        population = select_best_from_population(population, t_size)
        # [ END ]

        # --------------------------------------------------------------------------------------------

        # [ CROSSOVER ]
        parents_to_crossing = get_parents_to_crossover(population, p_k)

        if None in parents_to_crossing:
            print(population)

        for (pA, pB, idxA, idxB) in parents_to_crossing:
            population = make_crossover(population, pA, pB, idxA, idxB, t_max, d, pois, chargers, modifier)
        # [ END ]

        # --------------------------------------------------------------------------------------------

        # [ MUTATIONS ]
        parents_to_mutation = get_parents_to_mutation(population, p_m)

        if None in parents_to_mutation:
            print(population)

        for parent in parents_to_mutation:
            parent.make_mutation(random.random())
        # [ END ]

        # --------------------------------------------------------------------------------------------

        # [ STOP CONDITION ]
        best_parent = get_best_parent(population)

        best_values.append(
            (best_parent.value, best_parent.fitness(), best_parent.get_route_distance(), best_parent.route.copy())
        )

        if best_parent.value > best_value:
            best_value = best_parent.value
            counter = 0
        else:
            counter += 1

        best_parent.check()

        if counter >= 10:
            # print('PEEK FOUND at:', i - 9)
            break
        # [ END ]

    time = datetime.now() - now

    best_data = None
    for item in best_values:
        if best_data is None:
            best_data = item
        elif item[0] > best_data[0]:
            best_data = item

    # print('Iteration ended with max value = {}'.format(best_data[0]))
    # print(time)

    first = best_data[3][0][0]
    tour = [(first.lat, first.lon, first.value), ]

    for trip in best_data[3]:
        for i in range(1, len(trip)):
            poi = trip[i]
            tour.append(
                (poi.lat, poi.lon, poi.value)
            )

    return {
        'data': {
            'M': t_max,
            'D': d,
            'P_SIZE': p_size,
            'T_SIZE': t_size,
            'N': n,
            'iterations': len(best_values),
            'fitness': best_data[1],
            'distance': best_data[2],
            'value': best_data[0],
            'total_time': time
        },
        'best_values': best_values,
        'tour': tour,
    }
