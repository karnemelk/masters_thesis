from math import sqrt, sin, cos, atan2, pi
from statistics import mean, median

import pandas as pd

from genetic.algorithm import run_algorithm as genetic
from hybrid.algorithm import run_algorithm as hybrid


def get_distance(point_a, point_b):
    lat1 = point_a[0] * pi / 180
    lon1 = point_a[1] * pi / 180
    lat2 = point_b[0] * pi / 180
    lon2 = point_b[1] * pi / 180

    dlat = lat2 - lat1
    dlon = lon2 - lon1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * (sin(dlon / 2) ** 2)
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = 6371 * c

    return distance


def generate_stats(algorithm, n=10):
    p_m = 0.4
    p_k = 0.4

    data = []

    input_data = [
        {'m': 120, 'd': 0, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': 'data.csv'},
        {'m': 150, 'd': 0, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': 'data.csv'},
        {'m': 170, 'd': 0, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': 'data.csv'},
        {'m': 210, 'd': 0, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': 'data.csv'},
        {'m': 420, 'd': 0, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': 'data.csv'},
        {'m': 120, 'd': 1, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': 'data.csv'},
        {'m': 150, 'd': 1, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': 'data.csv'},
        {'m': 170, 'd': 1, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': 'data.csv'},
        {'m': 210, 'd': 1, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': 'data.csv'},
        {'m': 420, 'd': 1, 'p_size': 50, 't_size': 5, 'n': 100, 'p_m': p_m, 'p_k': p_k, 'path': 'data.csv'}
    ]

    for input in input_data:
        profits = []
        times = []
        for _ in range(n):
            result = algorithm(**input)
            profits.append(result['data']['value'])
            times.append(result['data']['total_time'].total_seconds())
        data.append({
            'd': input['d'],
            'm': input['m'],
            'profit': {
                'min': min(profits),
                'max': max(profits),
                'avg': mean(profits),
                'median': median(profits)
            },
            'time': {
                'min': min(times),
                'max': max(times),
                'avg': mean(times),
                'median': median(times)
            }
        })
        print('Input processed ...')

    return pd.json_normalize(data, max_level=1)


if __name__ == '__main__':
    generate_stats(hybrid, n=100).to_csv('hybrid_statistics.csv', index=False)
