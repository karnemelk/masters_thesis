from datetime import datetime

import numpy as np
import pandas as pd

from global_utils import make_pois_from_df, if_points_equal, dfs
# from global_utils import get_distance
from global_utils import get_eucl_dist as get_distance
from greedy.utils import perform_for_section, get_trip_distance, try_insert, try_replace


def run_algorithm(start, end, d, t_max, modifier, path, with_2opt=False, replace=False, insert=False, df=None):
    now = datetime.now()

    if df is not None:
        pois, chargers = make_pois_from_df(df)
    else:
        pois, chargers = make_pois_from_df(pd.read_csv(path))

    attractions_to_attractions = get_distances(pois, pois, modifier)
    chargers_to_attractions = get_distances(chargers, pois, modifier)

    tour = []

    is_poi_visited = [False] * len(pois)
    is_charger_visited = [False] * len(chargers)

    first_charger = chargers[start]
    last_charger = chargers[end]

    chargers = np.delete(chargers, max(start, end))
    chargers = np.delete(chargers, min(start, end))

    result, ready_chargers = dfs(first_charger, chargers, is_charger_visited, 0, d, t_max, last_charger)

    if not result:
        print('Tour cannot be created!')
        return {'data': {'value': 0, 'time': datetime.now() - now}, 'tour': []}

    # [ MAIN ALGORITHM ]
    for i in range(d + 1):
        trip = [ready_chargers[i], ]
        last_charger = ready_chargers[i + 1]
        recently_charged = True
        actual = trip[0]
        part = 0

        while True:
            # [ GET CANDIDATE POINT ]
            j = 0

            try:
                if recently_charged:
                    while is_poi_visited[chargers_to_attractions[actual.pk][j]['point'].pk]:
                        j += 1
                    candidate = chargers_to_attractions[actual.pk][j]['point']
                    recently_charged = False
                else:
                    while is_poi_visited[attractions_to_attractions[actual.pk][j]['point'].pk]:
                        j += 1
                    candidate = attractions_to_attractions[actual.pk][j]['point']
            except IndexError:
                break

            dist_left = get_distance(actual, candidate)
            dist_right = get_distance(candidate, last_charger)

            # [ CHECK VALIDATION ]
            if part + dist_left + dist_right <= t_max[i]:
                part += dist_left

                is_poi_visited[candidate.pk] = True
                trip.append(candidate)

                actual = candidate

                # [ 2-OPT OPERATOR EXECUTION ]
                if with_2opt:
                    trip, part = perform_for_section(trip, part)
            else:
                break

        part += get_distance(trip[-1], last_charger)
        trip.append(last_charger)

        # [ 2-OPT OPERATOR EXECUTION ]
        if with_2opt:
            trip, part = perform_for_section(trip, part)

        tour.append(trip)

    # [ REPLACE OPERATOR EXECUTION ]
    if replace:
        while try_replace(pois, is_poi_visited, tour, t_max, modifier):
            pass

    # [ INSERT OPERATOR EXECUTION ]
    if insert:
        while try_insert(pois, is_poi_visited, tour, t_max, modifier):
            pass

    first = tour[0][0]
    final = [(first.lat, first.lon, first.value), ]

    for trip in tour:
        for i in range(1, len(trip)):
            poi = trip[i]
            final.append(
                (poi.lat, poi.lon, poi.value)
            )

    time = datetime.now() - now

    data = {
        'data': {
            'm': t_max,
            'd': d,
            'modifier': modifier,
            'distance': sum([get_trip_distance(trip) for trip in tour]),
            'value': sum([sum([poi.value for poi in trip]) for trip in tour]),
            'time': time
        },
        'tour': final
    }

    return data


def get_distances(points, points_to_sort, modifier, reverse=True):
    distances = []

    for base_point in points:
        distances_per_point = []

        for point in points_to_sort:
            if not if_points_equal(base_point, point):
                distances_per_point.append({
                    'fitness': point.value ** modifier / get_distance(base_point, point)
                    if reverse else get_distance(base_point, point),
                    'point': point
                })

        distances_per_point = sorted(distances_per_point, key=lambda x: x['fitness'], reverse=reverse)
        distances.append(distances_per_point)

    return distances
