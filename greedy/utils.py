# from global_utils import get_distance
from global_utils import get_eucl_dist as get_distance


def perform_2opt(trip):
    if len(trip) < 4:
        return trip

    for j in range(3, len(trip)):
        trip, _ = perform_for_section(trip, 0)

    return trip


def perform_for_section(trip, dist):
    if len(trip) < 4:
        return trip, dist

    index = len(trip) - 1

    last1 = index - 1
    last2 = index

    for i in range(index - 2):
        first1 = i
        first2 = i + 1

        dist_old_1 = get_distance(trip[first1], trip[first2])
        dist_old_2 = get_distance(trip[last1], trip[last2])

        dist_new_1 = get_distance(trip[first1], trip[last1])
        dist_new_2 = get_distance(trip[first2], trip[last2])

        if dist_old_1 + dist_old_2 > dist_new_1 + dist_new_2:
            i_left = int(first2)
            i_right = int(last1)
            while i_left < i_right:
                temp = trip[i_left]
                trip[i_left] = trip[i_right]
                trip[i_right] = temp

                i_left += 1
                i_right -= 1

            return trip, dist - (dist_old_1 + dist_old_2) + dist_new_1 + dist_new_2

    return trip, dist


def try_insert(pois, is_poi_visited, tour, t_max, modifier):
    best_candidate = None
    candidate_fitness = 0
    trip_index = None
    position = None

    for candidate in pois:
        if is_poi_visited[candidate.pk]:
            continue

        for i in range(len(tour)):
            trip = tour[i]
            dist = get_trip_distance(trip)

            for j in range(1, len(trip)):
                distance_given = get_distance(trip[j - 1], candidate) + get_distance(candidate, trip[j])
                distance_removed = get_distance(trip[j - 1], trip[j])

                if dist - distance_removed + distance_given <= t_max[i]:
                    try:
                        fitness = (candidate.value ** modifier) / distance_given
                        if fitness > candidate_fitness:
                            best_candidate = candidate
                            candidate_fitness = fitness
                            trip_index = i
                            position = j
                    except ZeroDivisionError:
                        continue

    if best_candidate:
        is_poi_visited[best_candidate.pk] = True
        tour[trip_index].insert(position, best_candidate)
        tour[trip_index] = perform_2opt(tour[trip_index])
        return True
    else:
        return False


def try_replace(pois, is_poi_visited, tour, t_max, modifier):
    tour_index = None
    position = None
    lowest_fitness = 1e9

    for i in range(len(tour)):
        trip = tour[i]

        for j in range(1, len(trip) - 1):
            distance_left = get_distance(trip[j - 1], trip[j])
            distance_right = get_distance(trip[j], trip[j + 1])
            fitness = trip[j].value ** modifier / (distance_left + distance_right)

            if fitness < lowest_fitness:
                lowest_fitness = fitness
                tour_index = i
                position = j

    if position:
        return try_replace_with_best(pois, is_poi_visited, tour, t_max, modifier, lowest_fitness, tour_index, position)
    else:
        return False


def try_replace_with_best(pois, is_poi_visited, tour, t_max, modifier, lowest_fitness, tour_index, position):
    trip = tour[tour_index]

    dist = get_trip_distance(trip)
    old_left = get_distance(trip[position - 1], trip[position])
    old_right = get_distance(trip[position], trip[position + 1])

    best_candidate = None
    best_fitness = 0

    for candidate in pois:
        if is_poi_visited[candidate.pk]:
            continue

        distance_left = get_distance(trip[position - 1], candidate)
        distance_right = get_distance(candidate, trip[position + 1])
        fitness = candidate.value ** modifier / (distance_left + distance_right)

        new_dist = dist - (old_left + old_right) + (distance_left + distance_right)

        if fitness > lowest_fitness and fitness > best_fitness and new_dist <= t_max[tour_index]:
            best_fitness = fitness
            best_candidate = candidate

    if best_candidate:
        # print('[ FOUND FOR REPLACE ]', best_candidate.value, trip[position].value)
        is_poi_visited[trip[position].pk] = False
        is_poi_visited[best_candidate.pk] = True

        trip[position] = best_candidate

        return True
    else:
        return False


def get_trip_distance(trip):
    dist = 0

    for i in range(1, len(trip)):
        dist += get_distance(trip[i - 1], trip[i])

    return dist
