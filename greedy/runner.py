import pandas as pd

from greedy.algorithm import run_algorithm


path = '../data.csv'


def run(single=False):
    input_data = [
        {'m': 120, 'd': 0, 'modifier': 1.1},
        {'m': 150, 'd': 0, 'modifier': 1.1},
        {'m': 170, 'd': 0, 'modifier': 1.1},
        {'m': 210, 'd': 0, 'modifier': 1.1},
        {'m': 420, 'd': 0, 'modifier': 1.1},
        {'m': 120, 'd': 1, 'modifier': 1.1},
        {'m': 150, 'd': 1, 'modifier': 1.1},
        {'m': 170, 'd': 1, 'modifier': 1.1},
        {'m': 210, 'd': 1, 'modifier': 1.1},
        {'m': 420, 'd': 1, 'modifier': 1.1}
    ]

    keys = ['m', 'd', 'modifier', 'distance', 'value', 'time']
    start = 0
    end = 2

    if single:
        m = 120
        d = 0
        modifier = 1.2

        result = run_algorithm(start, end, d, [m] * (d + 1), modifier, path, with_2opt=True, replace=True, insert=True)
        print(result['data'])
    else:
        results = []
        for input in input_data:
            result = run_algorithm(
                start, end, input['d'], input['m'], input['modifier'], path,
                with_2opt=True, replace=True, insert=True
            )
            results.append(result['data'])
        df = pd.DataFrame(results, columns=keys)
        print(df)
        # df.to_csv('results_greedy.csv')


if __name__ == '__main__':
    run(single=True)
